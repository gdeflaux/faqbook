PDFKit.configure do |config|
  if Rails.env.development?
    config.wkhtmltopdf = '/usr/local/bin/wkhtmltopdf'
  else
    config.wkhtmltopdf = Rails.root.join('bin', 'wkhtmltopdf-linux-amd64').to_s
  end
  
  config.default_options = {
   :page_size => 'A4',
  }
  #config.root_url = "http://localhost" # Use only if your external hostname is unavailable on the server.
end