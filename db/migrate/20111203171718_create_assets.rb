class CreateAssets < ActiveRecord::Migration
  def change
    create_table :assets do |t|
      t.string :file
      t.integer :question_id

      t.timestamps
    end
  end
end
