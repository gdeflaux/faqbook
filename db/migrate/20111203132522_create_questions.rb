class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :title
      t.text :answer
      t.integer :chapter_id
      t.integer :sequence

      t.timestamps
    end
  end
end
