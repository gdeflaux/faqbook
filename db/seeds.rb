# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

puts "Creating dumb chapter..."
if Chapter.count == 0
  c1 = Chapter.create(:title => "Chapter 1", :sequence => 1)
  Question.create(:title => "Question 1", :sequence => 1, :answer => "Bla Bla Bla", :chapter => c1)
  puts "Dumb chapter created!"
else
  puts "A chapter already exists!"
end

puts ""

puts "Creating Admin user..."
if User.find_by_email("admin@faqbook.org").nil?
  User.create({ :email => 'admin@faqbook.org', :password => 'admin', :password_confirmation => 'admin', :role => "super_admin"})
  puts "Admin user created!"
else
  puts "Admin user already exists!"
end

puts ""

puts "Creating settings record..."
if Setting.count == 0
  Setting.create(:information => "Some contact information. This is *Textilized!*", :technical_contact => "person-receiving-error-msg@your-domain.com")
  puts "Settings record created!"
else
  puts "Settings record already exists!"
end

puts ""

puts "Creating default languages..."
if Language.count == 0
  Language.create(:name => "English", :abbreviation => "en")
  Language.create(:name => "Espanol", :abbreviation => "es")
  Language.create(:name => "Francais", :abbreviation => "fr")
  puts "Language records created! (en, fr, es)"
else
  puts "Languages already exist!"
end
