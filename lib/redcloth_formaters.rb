module RedclothFormaters
  module RedCloth::Formatters::HTML
    def table_open(opts)
      opts[:class] = "" if opts[:class].nil?
      opts[:class] = opts[:class] + " table table-condensed table-bordered"
      "<table#{pba(opts)}>\n"
    end
    
    def image(opts)  
      align = opts[:align] || "left"
      opts.delete(:align)
      opts[:alt] = opts[:title]
      asset = nil
      
      asset = @question.find_asset(opts[:src]) if @question
      if asset.nil?
        opts[:src]
      else
        
        if Rails.env.development?
          src = @pdf ? Rails.root.join("public#{asset.file.url(:pdf)}").to_s : asset.file.url(:normal)
        else
          src = asset.file.url(:normal)
        end
      end
      
      img = "<p class=\"img-#{align}\"><img src=\"#{escape_attribute src}\" #{pba(opts)} alt=\"#{escape_attribute opts[:alt].to_s}\"/></p>"
      img = "<a href=\"#{escape_attribute src}\">#{img}</a>" if opts[:href]
      img
    end  
  end
end
