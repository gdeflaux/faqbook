class TextileQuestion < RedCloth::TextileDoc
  def initialize(question, pdf, restrictions = [])
    @question = question
    @pdf = pdf
    super(question.answer, restrictions)
  end
end