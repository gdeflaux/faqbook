class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :set_locale

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end
  
  # app/controllers/application_controller.rb
  def default_url_options(options={})
    { :locale => I18n.locale }
  end
  
  def after_sign_in_path_for(resource)
    root_path
  end
  
  def after_sign_out_path_for(resource)
    root_path
  end
  
  def pdf_css_image_path(s)
    s.gsub(/url\("/, "url(\"#{Rails.root.join("app/assets/images/")}")
  end
  
  def add_breadcrumb(text, link, option = {})
    @breadcrumb = [] if !defined? @breadcrumb
    @breadcrumb << [text, link, option]
  end
  
end
