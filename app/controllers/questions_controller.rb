class QuestionsController < ApplicationController
  before_filter :authenticate_user!
  authorize_resource
  
  # GET /questions/new
  # GET /questions/new.json
  def new
    @chapter = Chapter.find(params[:chapter_id])
    if params[:locale] != "en"
      redirect_to @chapter, :alert => "New content must be created English!"
      return
    end
    @question = Question.new
    add_breadcrumb t('toc'), chapters_path
    add_breadcrumb @chapter.title, chapter_path(@chapter)
    add_breadcrumb t('breadcrumb.new_question'), ""

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @question }
    end
  end

  # GET /questions/1/edit
  def edit
    @chapter = Chapter.find(params[:chapter_id])
    @question = Question.find(params[:id])
    add_breadcrumb t('toc'), chapters_path
    add_breadcrumb @chapter.title, chapter_path(@chapter)
    add_breadcrumb @question.title, ""
    add_breadcrumb t('breadcrumb.edit'), ""
  end

  # POST /questions
  # POST /questions.json
  def create
    @chapter = Chapter.find(params[:chapter_id])
    @question = Question.new(params[:question])
    add_breadcrumb t('toc'), chapters_path
    add_breadcrumb @chapter.title, chapter_path(@chapter)
    add_breadcrumb t('breadcrumb.new_question'), ""

    respond_to do |format|
      if @question.save
        @question.insert_at(params[:question][:sequence].to_i)
        format.html { redirect_to @question.chapter, notice: 'Question was successfully created.' }
        format.json { render json: @question, status: :created, location: @question }
      else
        format.html { render action: "new" }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /questions/1
  # PUT /questions/1.json
  def update
    @chapter = Chapter.find(params[:chapter_id])
    @question = Question.find(params[:id])
    add_breadcrumb t('toc'), chapters_path
    add_breadcrumb @chapter.title, chapter_path(@chapter)
    add_breadcrumb @question.title, ""
    add_breadcrumb t('breadcrumb.edit'), ""
    respond_to do |format|
      if @question.update_attributes(params[:question])
        @question.insert_at(params[:question][:sequence].to_i)
        format.html { redirect_to @question.chapter, notice: 'Question was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /questions/1
  # DELETE /questions/1.json
  def destroy
    @question = Question.includes(:chapter).find(params[:id])
    @chapter = @question.chapter
    @question.remove_from_list
    @question.destroy

    respond_to do |format|
      format.html { redirect_to @chapter, notice: 'Question was successfully deleted.'}
      format.json { head :ok }
    end
  end
end
