class ChaptersController < ApplicationController
  include RedclothFormaters
  before_filter :authenticate_user!
  authorize_resource
  
  # GET /chapters
  # GET /chapters.json
  def index  
    add_breadcrumb t('toc'), chapters_path
    
    @chapters = Chapter.includes(:questions).order("chapters.sequence ASC")
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @chapters }
      format.pdf {
        html = ""
        html += render_to_string(:layout => false , :action => "cover.html.haml")
        @chapters.each do |chapter|
          @chapter = chapter
          html += render_to_string(:layout => false , :action => "show.html.haml")
        end
        engine = Sass::Engine.for_file("#{Rails.root}/app/assets/stylesheets/pdf.css.scss", :syntax => :scss)
        css = pdf_css_image_path(engine.render)
        html = "<style>\n" + css + "\n</style>\n" + html
        kit = PDFKit.new(html)
        send_data(kit.to_pdf, :filename => "tdh-handbook", :type => 'application/pdf')
        return
      }
    end
  end
  
  def search
  end

  # GET /chapters/1
  # GET /chapters/1.json
  def show
    @chapter = Chapter.includes(:questions).find(params[:id])
    @next = Chapter.find_by_sequence(@chapter.sequence + 1)
    @prev = Chapter.find_by_sequence(@chapter.sequence - 1)

    add_breadcrumb t('toc'), chapters_path
    add_breadcrumb @chapter.title, @chapter.question_links

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @chapter }
      format.pdf {
        html = render_to_string(:layout => false , :action => "show.html.haml")
        engine = Sass::Engine.for_file("#{Rails.root}/app/assets/stylesheets/pdf.css.scss", :syntax => :scss)
        css = pdf_css_image_path(engine.render)
        html = "<style>\n" + css + "\n</style>\n" + html
        kit = PDFKit.new(html)
        send_data(kit.to_pdf, :filename => @chapter.pdf_filename, :type => 'application/pdf')
        return
      }
    end
  end

  # GET /chapters/new
  # GET /chapters/new.json
  def new
    if params[:locale] != "en"
      redirect_to chapters_url, :alert => "New content must be created English!"
      return
    end
    @chapter = Chapter.new
    add_breadcrumb t('toc'), chapters_path
    add_breadcrumb t('breadcrumb.chapters'), chapters_path
    add_breadcrumb t('breadcrumb.new'), ""
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @chapter }
    end
  end

  # GET /chapters/1/edit
  def edit
    @chapter = Chapter.find(params[:id])
    add_breadcrumb t('toc'), chapters_path
    add_breadcrumb @chapter.title, chapter_path(@chapter)
    add_breadcrumb t('breadcrumb.edit'), ""
  end

  # POST /chapters
  # POST /chapters.json
  def create
    @chapter = Chapter.new(params[:chapter])
    add_breadcrumb t('toc'), chapters_path
    add_breadcrumb t('breadcrumb.chapters'), chapters_path
    add_breadcrumb t('breadcrumb.new'), ""
    
    respond_to do |format|
      if @chapter.save
        @chapter.insert_at(params[:chapter][:sequence].to_i)
        format.html { redirect_to @chapter, notice: 'Chapter was successfully created.' }
        format.json { render json: @chapter, status: :created, location: @chapter }
      else
        format.html { render action: "new" }
        format.json { render json: @chapter.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /chapters/1
  # PUT /chapters/1.json
  def update
    @chapter = Chapter.find(params[:id])
    add_breadcrumb t('toc'), chapters_path
    add_breadcrumb @chapter.title, chapter_path(@chapter)
    add_breadcrumb t('breadcrumb.edit'), ""
    respond_to do |format|
      if @chapter.update_attributes(params[:chapter])
        @chapter.insert_at(params[:chapter][:sequence].to_i)
        format.html { redirect_to @chapter, notice: 'Chapter was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @chapter.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /chapters/1
  # DELETE /chapters/1.json
  def destroy
    @chapter = Chapter.find(params[:id])
    @chapter.remove_from_list
    @chapter.destroy

    respond_to do |format|
      format.html { redirect_to chapters_url }
      format.json { head :ok }
    end
  end
end
