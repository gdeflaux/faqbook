class Admin::LanguagesController < ApplicationController
  before_filter :authenticate_user!
  authorize_resource
  # GET /languages
  # GET /languages.json
  def index
    @languages = Language.all
    add_breadcrumb t('breadcrumb.admin'), admin_path
    add_breadcrumb t('breadcrumb.languages'), ""

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @languages }
    end
  end

  # GET /languages/new
  # GET /languages/new.json
  def new
    @language = Language.new
    add_breadcrumb t('toc'), admin_path
    add_breadcrumb t('breadcrumb.languages'), admin_languages_path
    add_breadcrumb t('breadcrumb.new'), ""
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @language }
    end
  end

  # GET /languages/1/edit
  def edit
    @language = Language.find(params[:id])
    add_breadcrumb t('toc'), admin_path
    add_breadcrumb t('breadcrumb.languages'), admin_languages_path
    add_breadcrumb t('breadcrumb.edit'), ""
  end

  # POST /languages
  # POST /languages.json
  def create
    @language = Language.new(params[:language])
    add_breadcrumb t('toc'), admin_path
    add_breadcrumb t('breadcrumb.languages'), admin_languages_path
    add_breadcrumb t('breadcrumb.new'), ""
    respond_to do |format|
      if @language.save
        format.html { redirect_to admin_languages_url, notice: 'Language was successfully created.' }
        format.json { render json: @language, status: :created, location: @language }
      else
        format.html { render action: "new" }
        format.json { render json: @language.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /languages/1
  # PUT /languages/1.json
  def update
    @language = Language.find(params[:id])
    add_breadcrumb t('toc'), admin_path
    add_breadcrumb t('breadcrumb.languages'), admin_languages_path
    add_breadcrumb t('breadcrumb.edit'), ""
    respond_to do |format|
      if @language.update_attributes(params[:language])
        format.html { redirect_to admin_languages_url, notice: 'Language was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @language.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /languages/1
  # DELETE /languages/1.json
  def destroy
    @language = Language.find(params[:id])

    respond_to do |format|
      if Language.count == 1
        format.html { redirect_to admin_languages_url, :alert => "You cannot delete the last language" }
        format.json { head :ko }
      elsif (@language.abbreviation == "en")
        format.html { redirect_to admin_languages_url, :alert => "You cannot delete the default language (English)" }
        format.json { head :ko }
      else
        @language.destroy
        format.html { redirect_to admin_languages_url, :notice => "Language deleted successfully" }
        format.json { head :ok }
      end
    end
  end
end
