class Admin::UsersController < ApplicationController
  before_filter :authenticate_user!
  authorize_resource
  
  def index
    @users = User.all
    add_breadcrumb t('breadcrumb.admin'), admin_path
    add_breadcrumb t('breadcrumb.users'), ""
  end
  
  def edit
    @user = User.find(params[:id])
    add_breadcrumb t('breadcrumb.admin'), admin_path
    add_breadcrumb t('breadcrumb.users'), admin_users_path
    add_breadcrumb t('breadcrumb.edit'), ""
  end
  
  def new
    @user = User.new
    add_breadcrumb t('breadcrumb.admin'), admin_path
    add_breadcrumb t('breadcrumb.users'), admin_users_path
    add_breadcrumb t('breadcrumb.new'), ""
  end
  
  def update
    @user = User.find(params[:id])
    add_breadcrumb t('breadcrumb.admin'), admin_path
    add_breadcrumb t('breadcrumb.users'), admin_users_path
    add_breadcrumb t('breadcrumb.edit'), ""
    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to admin_users_url, notice: 'User was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def create
    @user = User.new(params[:user])
    add_breadcrumb t('breadcrumb.admin'), admin_path
    add_breadcrumb t('breadcrumb.users'), admin_users_path
    add_breadcrumb t('breadcrumb.new'), ""
    respond_to do |format|
      if @user.save
        format.html { redirect_to admin_users_url, notice: 'User was successfully created.' }
        format.json { render json: admin_users_url, status: :created, location: @chapter }
      else
        format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
  def destroy
    @user = User.find(params[:id])

    respond_to do |format|
      if User.count == 1
        format.html { redirect_to admin_users_url, alert: 'Cannot delete last user!'}
      else
        @user.destroy
        format.html { redirect_to admin_users_url, notice: 'User was successfully deleted.'}
        format.json { head :ok }
      end
    end
  end
end