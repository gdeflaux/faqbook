class Admin::SettingsController < ApplicationController
  before_filter :authenticate_user!
  authorize_resource
  
  def edit
    @setting = Setting.first
    add_breadcrumb t('breadcrumb.admin'), admin_path
    add_breadcrumb t('breadcrumb.settings'), ""
    add_breadcrumb t('breadcrumb.edit'), ""
  end
  
  def update
    @setting = Setting.first
    add_breadcrumb t('breadcrumb.admin'), admin_path
    add_breadcrumb t('breadcrumb.settings'), ""
    add_breadcrumb t('breadcrumb.edit'), ""
    respond_to do |format|
      if @setting.update_attributes(params[:setting])
        format.html { redirect_to chapters_url, notice: 'Chapter was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @setting.errors, status: :unprocessable_entity }
      end
    end
  end
end