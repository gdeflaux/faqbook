class Question < ActiveRecord::Base
  belongs_to :chapter
  has_many :assets, :dependent => :destroy
  accepts_nested_attributes_for :assets, :allow_destroy => true
  
  validates_uniqueness_of :title, :scope => :chapter_id
  validates_presence_of :title, :sequence, :answer, :chapter_id
  
  acts_as_list :column => "sequence", :scope => :chapter
  translates :title, :answer
  
  def anchor
    "q-" + title.gsub(/\W+/, '-').chomp("-").downcase
  end
  
  def find_asset(title)
    Asset.where(:question_id => id, :file => title).first
  end
  
  def self.sequence_select(chapter_id)
    count = Question.where("chapter_id = #{chapter_id}").count
    return [["First", 1]] if count == 0
    return [["First", 1], ["Last", 2]] if count == 1
    a = (2..count).to_a.map { |x| [x,x] } << ["Last", count + 1]
    a.insert(0, ["First", 1])
    a
  end

end
