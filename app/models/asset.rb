class Asset < ActiveRecord::Base
  attr_accessible :file
  belongs_to :question
  validates_uniqueness_of :file, :scope => :question_id
  mount_uploader :file, AssetUploader
end
