class User < ActiveRecord::Base  
  ROLES = %w{super_admin reader}

  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :recoverable, :rememberable, :trackable
  
  # Setup accessible (or protected) attributes for your model
  attr_accessible  :email, :password, :password_confirmation, :remember_me, :role
  
  validates_presence_of :email
  validates_uniqueness_of :email
  validates_presence_of :password, :on => :create
  validates :password, :confirmation => true
  validates_presence_of :role
  
  def admin?
    role == "super_admin"
  end
end
