class Chapter < ActiveRecord::Base
  has_many :questions, :order => "sequence", :dependent => :destroy
  acts_as_list :column => "sequence"
  translates :title, :description
  
  validates_uniqueness_of :title
  validates_presence_of :title, :sequence
  
  def self.sequence_select
    count = Chapter.count
    return [["First", 1]] if count == 0
    return [["First", 1], ["Last", 2]] if count == 1
    a = (2..count).to_a.map { |x| [x,x] } << ["Last", count + 1]
    a.insert(0, ["First", 1])
    a
  end
  
  def question_links
    links = []
    questions.each do |q|
      links << {:text =>"#{self.sequence}.#{q.sequence} #{q.title}", :anchor => q.anchor}
    end
    links
  end
  
  def last_update
    q = Question.where("chapter_id = #{self.id}").maximum('updated_at') || updated_at
    c = updated_at
    c > q ? c.strftime('%Y/%m/%d') : q.strftime('%Y/%m/%d')
  end
  
  def pdf_filename
    "#{sequence}-" + title.gsub(/\W+/, '-').chomp("-").downcase
  end
  
  def fully_translated?
    locale = I18n.locale
    questions.each do |q|
      return false if !q.translated_locales.include?(locale)
    end
    return true
  end
end
