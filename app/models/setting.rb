class Setting < ActiveRecord::Base
  validates_presence_of :information, :technical_contact
  translates :information
  
  def self.information
    Setting.first.information
  end
  
  def self.technical_contact
    Setting.first.technical_contact
  end
end
