class Language < ActiveRecord::Base
  validates_presence_of :name, :abbreviation
  
  def default?
    abbreviation == "en"
  end

  def self.current_name
    Language.find_by_abbreviation(I18n.locale.to_s).name
  end

end
