module ApplicationHelper
  def last_update
    q = Question.maximum('updated_at')
    c = Chapter.maximum('updated_at')
    c > q ? c.strftime('%Y/%m/%d') : q.strftime('%Y/%m/%d')
  end
  
  def link_to_textile_help    
    link_to "Syntax Reference", "/textile_help.html", :class => "textile-help pull-right", :title => "Syntax Reference",
            :onclick => 'window.open("/textile_help.html", "", "resizable=yes, location=no, width=500, height=640, menubar=no, status=no, scrollbars=yes"); return false;'
  end

  def link_to_icon(text, url, icon, options = {})
    link_to "<span class=\"#{icon}\"></span> #{text}".html_safe, url, options
  end
  
  def clippy(text, bgcolor='#DDDDDD')
  html = <<-EOF
    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"
            width="100"
            height="14"
            id="clippy" >
    <param name="movie" value="/assets/clippy.swf"/>
    <param name="allowScriptAccess" value="always" />
    <param name="quality" value="high" />
    <param name="scale" value="noscale" />
    <param NAME="FlashVars" value="text=#{text}">
    <param name="bgcolor" value="#{bgcolor}">
    <embed src="/assets/clippy.swf"
           width="100"
           height="14"
           name="clippy"
           quality="high"
           allowScriptAccess="always"
           type="application/x-shockwave-flash"
           pluginspage="http://www.macromedia.com/go/getflashplayer"
           FlashVars="text=#{text}"
           bgcolor="#{bgcolor}"
    />
    </object>
  EOF
end
    
end
